
import './App.css';
import Header from './components/Header/Header';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Category from './components/Category/Category';
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import CreateCategory from './components/Category/CreateCategory';
import Quizzes from './components/Quiz/Quizzes';
import CreateQuiz from './components/Quiz/CreateQuiz';

function App() {
  return (
    <main>
      <BrowserRouter>
        <Switch>
          <Route exact path = "/" component= {Header}/>
          <Route exact path = "/category" component = {Category}/>
          <Route path = "/register" component = {Register}/>
          <Route path = "/login" component = {Login}/>
          <Route path = "/category/create" component = {CreateCategory}/>
          <Route exact path = "/quiz" component = {Quizzes}/>
          <Route path = "/quiz/create" component = {CreateQuiz}/>
        </Switch>
      </BrowserRouter>
    </main>
  );
}

export default App;
