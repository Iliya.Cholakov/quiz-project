import React from 'react';
import PropTypes from 'prop-types';

const AppError = ({ message }) => {

  return (
    <div className="AppError">
      <h1>{message}</h1>
    </div>
  )
};

Error.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;