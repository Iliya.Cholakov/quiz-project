import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import CategoriesList from "./CategoriesList";
import { getCategories } from "../requests/category";
import { useHistory } from "react-router";
// import Link from 'react-router-dom'

const Category = () => {
  const [categories, setCategories] = useState(null);

  useEffect(() => {
    getCategories().then((data) => setCategories(data));
  }, []);
  const history = useHistory();
  const createCategory = () => {
    let path = "/category/create";
    history.push(path);
  };
  return (
    <Container>
      <h1>All Categories</h1>
      {categories === null ? (
        <div>Categories are loading ...</div>
      ) : (
        <CategoriesList categories={categories} />
      )}
      <button onClick={createCategory}>Create Category</button>
    </Container>
  );
};

export default Category;
