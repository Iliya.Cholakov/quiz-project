import React, { useState } from "react";
import { useHistory } from "react-router";
import AppError from "../Error/AppError";
import credentialsLength from "../common/CredentialsLength";
import { createCategory } from "../requests/category";

const CreateCategory = () => {
  const history = useHistory();
  const [error, setError] = useState(null);
  const [categoryName, setCategoryName] = useState("");

  const triggerCreateCategory = () => {
    if (!categoryName 
        || categoryName.length < credentialsLength.categoryNameMinLength 
        || categoryName.length > credentialsLength.categoryNameMaxLength) {
        alert('Category Name must be between 2....255 symbols')
    };
    
    createCategory(categoryName)
    .then(data => {
        if(data.message) {
            (history.push('/category/create'))
            return alert(data.message)
        }})
    .catch (error => setError(error.message))
    .then(history.push('/'));
};

if (error) {
    return (
      <AppError message="error"/>
    );
}
  return (
    <div className="createCategory">
      <form className="createCategory-form">
        <h1>Create Category here</h1>
        <h4>Category Name</h4>
        <input
          type="text"
          placeholder="Enter Category Name"
          onChange={(e) => setCategoryName(e.target.value)}
          value={categoryName}
        ></input>
        <br />
        <br />
        <button
          className="button"
          variant="outline-light"
          type="submit"
          onClick={triggerCreateCategory}
        >
          Create Category
        </button>{" "}
        <br />
      </form>
    </div>
  );
};

export default CreateCategory;
