const categoriesList = ({ categories }) =>
  categories.map((cat) => (
    <div key={cat.id}>
      <div> {cat.categoryName}</div>
    </div>
  ));

export default categoriesList;
