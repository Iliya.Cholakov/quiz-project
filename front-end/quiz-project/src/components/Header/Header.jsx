import React from "react";
//import { useHistory } from "react-router";
import { Container } from "react-bootstrap";
import { useHistory } from "react-router";

const Header = () => {
  const history = useHistory();
  const login = () => {
    let path = "/login";
    history.push(path);
  };
  const register = () => {
    let path = "/register";
    history.push(path);
  };
  const categories = () => {
    let path = "/category";
    history.push(path);
  };
  const quizzes = () => {
    let path = "/quiz";
    history.push(path);
  };
  return (
    <Container>
      <h1>Quiz Project</h1>
      <button onClick={login}>Login</button>
      <button onClick={register}>Register</button>
      <button onClick={categories}>Categories</button>
      <button onClick={quizzes}>Quizzes</button>
    </Container>
  );
};

export default Header;
