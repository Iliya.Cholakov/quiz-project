import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import credentialsLength from '../common/CredentialsLength';
import { createUser } from '../requests/user';
import AppError from '../Error/AppError';
// import umbrellas_couch from './umbrellas_couch.png';
// import './Register.css';
 /*eslint no-unused-vars: "off"*/

const Register = () => {
    const history = useHistory();
    const [error, setError] = useState(null);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const triggerRegister = () => {
        // eslint-disable-next-line no-useless-escape
        const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

        if (!email 
            || email.length < credentialsLength.emailMinLength  
            || email.length > credentialsLength.emailMaxLength
            || email.search(emailRegex)) {
            alert('Please enter e-mail with at least 10 and max 25 symbols or correct email')
        };
        if (!password 
            || password.length < credentialsLength.passwordMinLength 
            || password.length > credentialsLength.passwordMaxLength) {
            alert('Password must be at least 6 and max 15 symbols')
        };
        if (!firstName 
            || firstName.length < credentialsLength.firstNameMinLength 
            || firstName.length > credentialsLength.firstNameMaxLength) {
            alert('First name must be at least 2 and max 15 symbols')
        };
        if (!lastName 
            || lastName.length < credentialsLength.lastNameMinLength 
            || lastName.length > credentialsLength.lastNameMaxLength) {
            alert('Last name must be at least 1 and max 15 symbols')
        };
        
        createUser(email, username, firstName, lastName, password)
        .then(data => {
            if(data.message) {
                (history.push('/register'))
                return alert(data.message)
            }})
        .catch (error => setError(error.message))
        .then(history.push('/'));
    };

    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return (
        <div className="register">
        <form className="register-form">
            <h1>Create your account here</h1>
            <h4>Select email</h4>
            <input type="email" placeholder="Enter email" onChange={e => setEmail(e.target.value)} value={email}></input>
            <br/><br/>
            <h4>Select password</h4>
            <input type="password" placeholder="Enter password" onChange={e => setPassword(e.target.value)} value={password}></input>
            <br/><br/>
            <h4>Select Username</h4>
            <input type="text" placeholder="Enter Username" onChange={e => setUsername(e.target.value)} value={username}></input>
            <br/><br/>
            <h4>Select first name</h4>
            <input type="text" placeholder="Enter first name" onChange={e => setFirstName(e.target.value)} value={firstName}></input>
            <br/><br/>
            <h4>Select last name</h4>
            <input type="text" placeholder="Enter last name" onChange={e => setLastName(e.target.value)} value={lastName}></input>
            <br/><br/>
            <button className="button" variant="outline-light" type="submit" onClick={triggerRegister}>
                Create account
            </button> <br/>
        </form>
        </div>
    )
};

export default Register;