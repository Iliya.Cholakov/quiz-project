const credentialsLength = {
    emailMinLength: 10,
    emailMaxLength: 25,
    passwordMinLength: 6,
    passwordMaxLength: 15,
    firstNameMinLength: 2,
    firstNameMaxLength: 15,
    lastNameMinLength: 1,
    lastNameMaxLength: 15,
    categoryNameMaxLength: 255,
    categoryNameMinLength: 2
}

export default credentialsLength;