import { BASE_URL } from "../common/constants"

export const createUser = (email, username, firstName, lastName, password) => {
  return fetch(`${BASE_URL}/user`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username,
      password,
      email,
      firstName,
      lastName,

    })
  })
    .then(res => res.json());
};