import { BASE_URL } from "../common/constants"

export const getCategories = () => fetch(`${BASE_URL}/category`).then(res => res.json())
export const createCategory = (categoryName) => {
    return fetch(`${BASE_URL}/category`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          categoryName
    
        })
      })
        .then(res => res.json());
}