DROP DATABASE if EXISTS quiz;

CREATE DATABASE quiz;

USE quiz;

CREATE TABLE Users (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(30) NOT NULL,
    email VARCHAR(30) NOT NULL UNIQUE,
    lastName VARCHAR(30) NOT NULL,
    firstName VARCHAR(30) NOT NULL,
    role ENUM ('student','teacher') NOT NULL
);

CREATE TABLE Category (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    categoryName VARCHAR(30) UNIQUE
);

CREATE TABLE Quiz (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    quizName VARCHAR(30) UNIQUE,
    categoryId INT,
    teacher INT,
    duration INT,
    FOREIGN KEY (categoryId) REFERENCES Category(id),
    FOREIGN KEY (teacher) REFERENCES Users(id)
);

CREATE TABLE UserQuiz (
    userId INT NOT NULL,
    quizId INT NOT NULL,
    score DECIMAL,
    startTime DATETIME,
    endTime DATETIME,
    FOREIGN KEY (userId) REFERENCES Users(id),
    FOREIGN KEY (quizId) REFERENCES Quiz(id),
    PRIMARY KEY (userId, quizId)
);

CREATE TABLE Questions (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    quizId INT,
    questionText VARCHAR(255),
    points INT,
    choiceType ENUM('MULTIPLE_CHOICE', 'SINGLE_CHOICE') NOT NULL,
    FOREIGN KEY (quizId) REFERENCES Quiz(id) ON DELETE CASCADE
);

CREATE TABLE PossibleAnswers (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    questionId INT,
    answer VARCHAR(255),
    isCorrect BOOLEAN,
    FOREIGN KEY (questionId) REFERENCES Questions(id) ON DELETE CASCADE
);

CREATE TABLE UserAnswer (
  userId INT NOT NULL,
  answerId INT NOT NULL,
  isCheckedAsCorrect BOOLEAN,
  FOREIGN KEY (userId) REFERENCES Users(id),
  FOREIGN KEY (answerId) REFERENCES PossibleAnswers(id),
  PRIMARY KEY (userId, answerId)
);
