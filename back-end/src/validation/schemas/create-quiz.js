const createQuizSchema = {
    quizName: value => {
        if (!value) {
            return { message: 'Quiz name is required' };
        }

        if (typeof value !== 'string' || value.length < 2 || value.length > 1000) {
            return { message: 'Quiz name should be a string in range [2..1000]' };
        }

        return null;
    },
    duration: value => {
        if (!value) {
            return { message: 'Duration is required' };
        }

        if (typeof value !== 'integer' || value <= 0 || value > 1500) {
            return { message: 'Duration should be an integer in range [0..1500]' };
        }

        return null;
    }
};

export default createQuizSchema;