const createUserSchema = {
    username: value => {
        if (!value) {
            return { message: 'Username is required' };
        }

        if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
            return { message: 'Username should be a string in range [3..25]' };
        }

        return null;
    },
    password: value => {
        if (!value) {
            return { message: 'Password is required' };
        }

        if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
            return { message: 'Password should be a string in range [3..25]' };
        }

        return null;
    },
    email: value => {
        if (!value) {
            return { message: 'E-mail is required' };
        }

        const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (typeof value !== 'string' || value.length < 10 || value.length > 25 || value.search(emailRegex)) {
            return { message: 'Invalid E-mail or should be a string in range [10..25]' };
        }

        return null;
    },
    firstName: value => {
        if (!value) {
            return { message: 'First name is required' };
        }

        if (typeof value !== 'string' || value.length < 2 || value.length > 15) {
            return { message: 'First name should be a string in range [2..15]' };
        }

        return null;
    },
    lastName: value => {
        if (!value) {
            return { message: 'Last name is required' };
        }

        if (typeof value !== 'string' || value.length < 1 || value.length > 15) {
            return { message: 'Last name should be a string in range [1..15]' };
        }

        return null;
    }
};

export default createUserSchema;