const createCategorySchema = {
    categoryName: value => {
        if (!value) {
            return { message: 'Category name is required' };
        }

        if (typeof value !== 'string' || value.length < 2 || value.length > 255) {
            return { message: 'Category name should be a string in range [2..255]' };
        }

        return null;
    }
};

export default createCategorySchema;