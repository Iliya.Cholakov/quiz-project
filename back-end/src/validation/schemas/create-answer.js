const createAnswerSchema = {
    answer: value => {
        if (!value) {
            return { message: 'Answer is required' };
        }

        if (typeof value !== 'string' || value.length < 3 || value.length > 1000) {
            return { message: 'Answer should be a string in range [3..1000]' };
        }

        return null;
    },
    isCorrect: value => {
        if (!value) {
            return { message: 'Answer must be checked as true or false' };
        }

        if (typeof value !== 'boolean') {
            return { message: 'Answer must be checked as true or false' };
        }

        return null;
    }
};
export default createAnswerSchema;