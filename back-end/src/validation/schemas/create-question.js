const createQuestionSchema = {
    questionText: value => {
        if (!value) {
            return { message: 'Question is required' };
        }

        if (typeof value !== 'string' || value.length < 3 || value.length > 1000) {
            return { message: 'Question should be a string in range [3..1000]' };
        }

        return null;
    },
    points: value => {
        if (!value) {
            return { message: 'Points is required' };
        }

        if (typeof value !== 'integer' || value.length < 1 || value.length > 6) {
            return { message: 'Points should be an integer in range [1..6]' };
        }

        return null;
    },
    choiceType: value => {
        if (!value) {
            return { message: 'Choice type is required' };
        }

        if (typeof value !== 'string' || value !== 'SINGLE_CHOICE' || value !== 'MULTIPLE_CHOICE') {
            return { message: 'Choice type should be either SINGLE_CHOICE or MULTIPLE_CHOICE' };
        }

        return null;
    }
};

export default createQuestionSchema;