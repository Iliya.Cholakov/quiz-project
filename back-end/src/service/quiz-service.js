import quizData from '../data/quiz-data.js'
import errorMessage from "../data/error.js";

const getAllQuizzes = async () => {
    return await quizData.getAllQuizzes();
}

const getQuizBy = async (column, value) => {

    if (column === 'id') {
        return await quizData.getQuizBy("id", value);
    } else if (column === 'categoryName') {
        return await quizData.getQuizzesByCategory("categoryName", value);
    } else if (column === 'quizName') {
        return await quizData.getQuizBy('quizName', value)
    }

}

const getQuestions = async (quizId) => {
    const quiz = await quizData.getQuizBy("id", quizId);
    if (!quiz) {
        return {
            error: errorMessage.NO_SUCH_QUIZ
        };
    }
    const questions = await quizData.getQuestions(quizId);

    for (const question of questions) {
        const answers = await quizData.getAnswersForQuestion(question.id);
        question.answers = answers;
    }

    return { questions: questions, error: null };
}

const createQuestion = async (quizId, data) => {
    const { questionText, points, choiceType, answers } = data;
    const quiz = await quizData.getQuizBy('id', quizId);
    if (!quiz) {
        return { error: errorMessage.NO_SUCH_QUIZ };
    }
    
    const newQuestion = await quizData.createQuestion(quizId, questionText, points, choiceType, answers);

    await quizData.createAnswersForQuestion(newQuestion.id, answers);

    return {
        newQuestion: newQuestion,
        answers: answers,
        error: null
    };
}

const createQuiz = async (data) => {
    const { quizName, categoryId, teacher, duration } = data;

    const quiz = await quizData.getQuizBy('quizName', quizName);
    if (quiz) {
        return { error: errorMessage.QUIZ_ALREADY_EXISTS };
    }

    const newQuiz = await quizData.createQuiz(quizName, categoryId, teacher, duration);

    return {
        newQuiz: newQuiz,
        error: null
    };
}

const deleteQuiz = async (id) => {
    const quiz = await getQuizBy('id', id);
    if (!quiz) {
        return { error: errorMessage.NO_SUCH_QUIZ, quiz: null };
    }

    const deleted = await quizData.deleteQuiz(quiz);
    return { quiz: quiz, error: null };
}

const getAllCategories = async () => {
    const categories = await quizData.getAllCategories();
    return { category: categories };
}

const getCategoryByName = async (categoryName) => {
    const category = await quizData.getCategoryBy('categoryName', categoryName);
    if (!category) {
        return {
            error: errorMessage.NO_SUCH_CATEGORY,
            category: null
        }
    }

    return { category: category, error: null };
}

const createCategory = async (categoryName) => {

    const category = await quizData.getCategoryBy('categoryName', categoryName);

    if (category) {
        return {
            error: errorMessage.CATEGORY_ALREADY_EXISTS,
            category: null
        }
    }
    const newCategory = await quizData.createCategory(categoryName);
    return { error: null, category: newCategory };
}

const updateCategory = async (id, quizzesData) => {
    const category = await quizData.getCategoryBy('id', id);
    if (!category) {
        return error.NO_SUCH_CATEGORY;
    }

    const updated = { ...category, ...quizzesData };
    const updatedCategory = await userData.update(updated);
    return updated;
}

const deleteCategory = async (id) => {
    const category = await quizData.getCategoryBy('id', id);
    if (!category) {
        return error.NO_SUCH_CATEGORY;
    }

    const deleted = await quizData.deleteCategory(category);
    return category;
}

export default {
    getAllQuizzes,
    getQuizBy,
    getQuestions,
    createQuiz,
    createQuestion,
    deleteQuiz,
    getAllCategories,
    getCategoryByName,
    createCategory,
    updateCategory,
    deleteCategory,

}