import examData from '../data/exam-data.js'
import quizData from '../data/quiz-data.js'
import usersData from '../data/users-data.js'
import errorMessage from '../data/error.js'

const startQuiz = async (userId, quizId) => {
    const quiz = await quizData.getQuizBy('id', quizId);
    if (!quiz) {
        return {
            quiz: null,
            error: errorMessage.NO_SUCH_QUIZ
        };
    }

    const user = await usersData.getUserBy('id', userId);
    if (!user) {
        return {
            quiz: null,
            error: errorMessage.NO_SUCH_USER
        };
    }
    const { startTime, endTime } = await examData.isQuizStarted(userId, quizId);
    if (endTime && user.role === 'student') {
        return { error: errorMessage.QUIZ_ALREADY_SUBMITTED };
    } else if (startTime) {
        return { error: errorMessage.QUIZ_ALREADY_STARTED };
    }

    const start = await examData.startQuiz(userId, quizId);
    return { message: 'Quiz successfully started', error: null };
}

const submitQuiz = async (userId, quizId, answers) => {
    const quiz = await quizData.getQuizBy('id', quizId);
    if (!quiz) {
        return {
            quiz: null,
            error: errorMessage.NO_SUCH_QUIZ
        };
    }

    const user = await usersData.getUserBy('id', userId);
    if (!user) {
        return {
            quiz: null,
            error: errorMessage.NO_SUCH_USER
        };
    }

    const { startTime, endTime } = await examData.isQuizStarted(userId, quizId);

    if (!startTime) {
        return { error: errorMessage.QUIZ_HAS_NOT_STARTED };
    } else if(endTime){
        return {error: errorMessage.QUIZ_ALREADY_SUBMITTED};
    }

    for (const answer of answers) {
        const submit = await examData.submitAnswer(userId, answer.id, answer.isCheckedAsCorrect);
    }
    await examData.submitQuiz(userId, quizId);
    const { score } = await calculateExamScore(userId, quizId);
    
    return { message: 'Successfully submitted', score: score };
}

const getExam = async (userId, quizId) => {
    const { startTime, endTime } = await examData.isQuizStarted(userId, quizId);
    const user = await usersData.getUserBy('id', userId);
    if (!user) {
        return {
            quiz: null,
            error: errorMessage.NO_SUCH_USER
        };
    }

    if (!startTime) {
        return { error: errorMessage.QUIZ_HAS_NOT_STARTED };
    } else if (endTime && user.role === 'student') {
        return { error: errorMessage.QUIZ_ALREADY_SUBMITTED };
    }

    const questions = await examData.getQuestions(quizId);

    for (const question of questions) {
        const answers = await examData.getAnswersForQuestion(question.id);
        question.answers = answers;
    }

    return { questions: questions, error: null };
}

const calculateExamScore = async (userId, quizId) => {

    const questions = await examData.getQuestions(quizId);

    let score = 0;
    for (const question of questions) {
        const questionAnswers = await examData.getAnswers(question.id);
        let match = true;
        for (const answer of questionAnswers) {
            const userExamAnswers = await examData.getUserAnswer(answer.id);

            if (userExamAnswers.isCheckedAsCorrect !== answer.isCorrect) {
                match = false;
                break;
            }
        }

        if (match) {
            score = score + question.points;
        }

    }
    const setScore = await examData.setScore(userId, quizId, score);

    return { score: score };

}

const getExamScore = async (userId, quizId) => {
    const user = await usersData.getUserBy('id', userId);
    if (!user) {
        return {
            score: null,
            error: errorMessage.NO_SUCH_USER
        };
    }

    const quiz = await quizData.getQuizBy('id', quizId);
    if (!quiz) {
        return {
            score: null,
            error: errorMessage.NO_SUCH_QUIZ
        };
    }

    const { score } = await examData.getScore(userId, quizId);

    return { error: null, score: score };
}
export default {
    startQuiz,
    submitQuiz,
    getExam,
    getExamScore,
    calculateExamScore,
}