import errorMessage from '../data/error.js';
import error from '../data/error.js';
import questionData from '../data/questions-data.js';
//import quizData from '../data/quiz-data.js'

const getAnswersByQuestionId = async (questionId) => {
    const question = await questionData.getQuestionBy("id", questionId);
    if (!question) {
        return { error: errorMessage.NO_SUCH_QUESTION, question: null };
    }
    const answers = await questionData.getAnswersByQuestionId(questionId);
    return { answers: answers };
}
const getQuestionById = async (id) => {
    const question = await questionData.getQuestionBy("id", id);
    if (!question) {
        return { error: errorMessage.NO_SUCH_QUESTION, question: null };
    }
    const { answers } = await getAnswersByQuestionId(id);

    question.answers = answers;

    return { question: question, error: null };
}

const createQuestion = async (data) => {
    const { quizId, questionText, categoryId, points, choiceType } = data;

    const newQuestion = await questionData.createQuestion(quizId, questionText, categoryId, points, choiceType);

    return newQuestion;
}

const createAnswer = async (questionId, data) => {
    const question = await getQuestionById(questionId);
    if (!question) {
        return { error: errorMessage.NO_SUCH_QUESTION }
    }

    const result = [];

    for (const element of data) {
        const answerExist = await questionData.getAnswersBy('answer', answer);
        if (!answerExist) {
            const answer = await questionData.createAnswer(questionId, element.answer, element.isCorrect);
            result.push(answer);
        }
    }

    return { result: result };
}

const updateQuestion = async (id, data) => {
    const question = await questionData.getQuestionBy('id', id);

    if (!question) {
        return error.NO_SUCH_QUESTION;
    }

    const updated = { ...question, ...data };

    const updatedQuestion = await questionData.updateQuestion(updated);

    return updatedQuestion;
}

const deleteAnswers = async (questionId) => {
    const question = await questionData.getQuestionBy('id', questionId);

    if (!question) {
        return error.NO_SUCH_QUESTION;
    }

    const deleted = await questionData.deleteAnswers(questionId);
    return { deleted: deleted, error: error }
}

const deleteQuestion = async (id) => {
    const question = await questionData.getQuestionBy('id', id);

    if (!question) {
        return error.NO_SUCH_QUESTION;
    }

    const deleted = await questionData.deleteQuestion(id);

    return { deleted: deleted, error: error }
}

export default {
    getAnswersByQuestionId,
    getQuestionById,
    createQuestion,
    createAnswer,
    updateQuestion,
    deleteQuestion,
    deleteAnswers

}