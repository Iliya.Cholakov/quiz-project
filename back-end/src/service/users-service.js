import errorMessage from '../data/error.js';
import userData from '../data/users-data.js';

const getAllUsers = async () => {
    return await userData.getAllUsers();
};

const getUserBy = async (column, value) => {
    if (column === 'id') {
        return await userData.getUserBy("id", value);
    } else if (column === 'username') {
        return await userData.getUserBy("username", value);
    } else if (column === 'email') {
        return await userData.getUserBy("email", value);
    } else {
        return error.NO_SUCH_USER;
    }

}

const createUser = async (usersData) => {
    const { username, password, email, firstName, lastName } = usersData;

    const user = await getUserBy('username', username);
    const role = 'student';
    if (user) {
        return { error: errorMessage.USER_ALREADY_EXISTS, user: null };
    }
    const newUser = await userData.create(username, password, email, firstName, lastName, role);

    return { error: null, user: newUser };
}
// update user not working
const updateUser = async (username, updateData) => {

    const user = await userData.getUserBy('username', username);
    if (!user) {
        return {
            error: errorMessage.NO_SUCH_USER,
            user: null
        }
    }

    if (updateData.username && !!(await userData.getUserBy('username', updateData.username))) {
        return {
            error: errorMessage.USERNAME_ALREADY_TAKEN,
            user: null
        }
    }

    const updated = { ...user, ...updateData };
    const updatedPerson = await userData.update(updated);
    return { error: null, user: updated }
}

const deleteUser = async (username) => {
    const user = await userData.getUserBy('username', username);
    if (!user) {
        return {
            error: errorMessage.NO_SUCH_USER,
            user: null
        }
    }

    await userData.remove(user);
    return { error: null, user: user };
}

const userLogin = async (email, password) => {
    const user = await userData.getUserBy('email', email);

        if (!user || !(await bcrypt.compare(password, user.password)) || !email) {
            return {
                error: errorMessage.INVALID_CREDENTIALS,
                user: null
            };
        }

        return {
            error: null,
            user: user
        };
}
export default {
    getAllUsers,
    getUserBy,
    createUser,
    updateUser,
    deleteUser,
    userLogin
}