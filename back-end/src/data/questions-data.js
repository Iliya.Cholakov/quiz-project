import connection from "./pool.js";

const getAnswersByQuestionId = async (id) => {
    const sql = `
    SELECT id, questionId, answer, isCorrect
    FROM possibleAnswers
    WHERE questionId = ?
    `;
    return await connection.query(sql, [id]);
}
const getQuestionBy = async (column,value) => {
    const sql = `
    SELECT id, quizId, questionText, points, choiceType
    FROM questions
    WHERE ${column} = ${value}
    `;
    const res = await connection.query(sql,[column, value]);
    return res[0];
}

const createQuestion = async (quizId, questionText, categoryId, points, choiceType) => {
    const sql = `
    INSERT INTO questions(quizId, questionText, categoryId, points, choiceType)
    VALUES (?,?,?,?,?)
`;

    const result = await connection.query(sql, [quizId, questionText, categoryId, points, choiceType]);

    return {
        id: result.insertId,
        questionText: questionText,
        choiceType: choiceType,
        points: points
    };
}

const createAnswer = async (questionId, answer, isCorrect) => {
    const sql = `
    INSERT INTO possibleAnswers(questionId, answer, isCorrect)
    VALUES (?,?,?)
`;

    const result = await connection.query(sql, [questionId, answer, isCorrect]);

    return {
        id: result.insertId,
        answer: answer,
        isCorrect: isCorrect
    };
}

const updateQuestion = async (question) => {
    const { quizId, questionText, categoryId, points, choiceType } = question
    const sql = `
    UPDATE question SET
     quizId = ?,
     questionText = ?,
     categoryId = ?,
     points = ?,
     choiceType = ?
     WHERE id = ?
    `;
    return await connection.query(sql, [quizId, questionText, categoryId, points, choiceType]);
}

const deleteQuestion = async (questionId) => {
    const sql = `
    DELETE FROM questions
    WHERE id = ${questionId}
    `;
    return await connection.query(sql);
}

const deleteAnswers = async (questionId) => {
    const sql = `
    DELETE FROM possibleAnswers
    WHERE questionId = ${questionId}
    `;
    return await connection.query(sql);
}

export default {
    getAnswersByQuestionId,
    getQuestionBy,
    createQuestion,
    createAnswer,
    updateQuestion,
    deleteQuestion,
    deleteAnswers
}