//import error from "./error.js";
import connection from "./pool.js"

const getAllUsers = async () => {
    const sql = `
        SELECT id, username, email, firstName, lastName, role 
        FROM users
    `; 

    return await connection.query(sql);
};

const getUserBy = async (column, value) => {
    const sql = `
    SELECT id, username, email, firstName, lastName, role
    FROM users
    WHERE ${column} = '${value}'
    `;
    const res = (await connection.query(sql,[column,value]))[0];
    if(res){
        return res;
    }
    return null;
}

const create = async (username, password, email, firstName, lastName, role) => {
    const sql = `
        INSERT INTO users(username, password, email, firstName, lastName, role)
        VALUES (?,?,?,?,?,?)
    `;

    const result = await connection.query(sql, [username, password, email, firstName, lastName, role]);

    return {
        id: result.insertId,
        email: email,
        firstName: firstName,
        lastName: lastName,
        role: role
    };
};

const update = async (person) => {
    const {id, username, email, firstName, lastName} = person
    const sql = `
    UPDATE users SET
     username = ?,
     email = ?,
     firstName = ?,
     lastName = ?
     WHERE id = ?
    `;
    return await connection.query(sql, [username, email, firstName, lastName, id]);
}

const remove = async (user) => {
    const sql = `
    DELETE FROM users
    WHERE username = ?
    `;
    return await connection.query(sql, [user.username]);
}

export default{
    getAllUsers,
    getUserBy,
    create,
    update,
    remove
}