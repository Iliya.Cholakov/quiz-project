const errorMessage = {
    NO_SUCH_USER: 'User does not exist',
    USER_ALREADY_EXISTS: 'User already exist',
    NO_SUCH_QUIZ: 'Quiz does not exist',
    QUIZ_ALREADY_EXISTS: 'Quiz already exist',
    NO_SUCH_CATEGORY: 'Category does not exist',
    CATEGORY_ALREADY_EXISTS: 'Category already exists',
    INVALID_INPUT_NAME: 'Name must be between 4 and 30 symbols',
    INVALID_INPUT_PASSWORD: 'Password must be at least 6 symbols',
    USERNAME_ALREADY_TAKEN: 'Username is already in use by another user',
    EMAIL_ALREADY_TAKEN: 'Email is already in use by another user',
    INVALID_EMAIL_FORMAT: 'Invalid email format',
    NO_SUCH_QUESTION: 'Question does not exist',
    QUESTION_ALREADY_EXISTS: 'Question already exists',
    QUIZ_ALREADY_STARTED: 'Quiz already started',
    NO_SUCH_QUIZ_STUDENT: 'Quiz/Student not found',
    QUIZ_ALREADY_SUBMITTED: 'Quiz is already submitted',
    QUIZ_HAS_NOT_STARTED: 'Quiz has not started',
    INVALID_CREDENTIALS: 'Wrong Username/Password'

}

export default errorMessage;