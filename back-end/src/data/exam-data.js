import connection from "./pool.js"

const startQuiz = async (userId, quizId) => {
    const sql = `
    INSERT INTO userQuiz (userId, quizId, startTime)
    VALUES (?,?,NOW())
`;
    const res = await connection.query(sql, [userId, quizId]);
    return {
        startTime: res.startTime
    };
}

const isQuizStarted = async (userId, quizId) => {
    const sql = `
    SELECT startTime, endTime FROM userQuiz
    WHERE userId = ${userId} AND quizId = ${quizId}
`;
    const res = (await connection.query(sql, [userId, quizId]))[0];
    if (res) {
        return {
            startTime: res.startTime,
            endTime: res.endTime
        };
    }
    return {
        startTime: null,
        endTime: null
    }

}

const submitAnswer = async (userId, answerId, isCheckedAsCorrect) => {
    const sql = `
    INSERT INTO userAnswer (userId, answerId, isCheckedAsCorrect)
    VALUES (?,?,?)
`;
    const res = await connection.query(sql, [userId, answerId, isCheckedAsCorrect]);
}

const submitQuiz = async (userId, quizId) => {
    const sql = `
    UPDATE userQuiz
    SET endTime = NOW()
    WHERE userId = ? AND quizId = ?
`;
    const res = await connection.query(sql, [userId, quizId]);
}

const getQuestions = async (quizId) => {
    const sql = `
    SELECT id, questionText, points, choiceType 
    FROM questions
    WHERE quizId = ${quizId};
    `
    const res = await connection.query(sql);
    return res;
}

const getAnswersForQuestion = async (questionId) => {
    const sql = `
    SELECT id, answer 
    FROM possibleAnswers 
    WHERE questionId = ${questionId};
    `
    const res = await connection.query(sql);
    return res;
}

const getAnswers = async (questionId) => {
    const sql = `
    SELECT id, answer, isCorrect
    FROM possibleAnswers 
    WHERE questionId = ${questionId};
    `
    const res = await connection.query(sql);
    return res;
}

const getUserAnswer = async (answerId) => {
    const sql = `
    SELECT isCheckedAsCorrect
    FROM userAnswer
    WHERE answerId = ${answerId};
    `
    const res = (await connection.query(sql))[0];
    return res;
}

const setScore = async(userId, quizId, score) => {
    const sql = `
    UPDATE userQuiz
    SET score = ${score}
    WHERE userId = ? AND quizId = ?
`;
    const res = await connection.query(sql, [userId, quizId]);
    return res;
}

const getScore = async (userId, quizId) => {
    const sql = `
    SELECT score
    FROM userQuiz
    WHERE userId = ${userId} AND quizId = ${quizId}
    `;
    const res = (await connection.query(sql))[0];
    return {
        score: res.score
    };
}

export default {
    startQuiz,
    isQuizStarted,
    submitAnswer,
    submitQuiz,
    getQuestions,
    getAnswersForQuestion,
    getAnswers,
    getUserAnswer,
    setScore,
    getScore
}