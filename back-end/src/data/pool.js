import mariadb from 'mariadb'
import { DB_CONFIG } from '../config.js';

const connection = mariadb.createPool(DB_CONFIG);

// const connection = pool.getConnection();

export default connection;