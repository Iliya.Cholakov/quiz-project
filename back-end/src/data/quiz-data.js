import connection from "./pool.js";

const getAllQuizzes = async () => {
    const sql = `
    SELECT id, quizName, categoryId, teacher, duration
    FROM quiz
`;

    return await connection.query(sql);
}

const getQuizBy = async (column, value) => {
    const sql = `
    SELECT * 
    FROM quiz 
    WHERE ${column} = '${value}';
    `
    const res = (await connection.query(sql, [column, value]))[0];
    return res;
}

const getQuizzesByCategory = async (column, value) => {
    const sql = `
    SELECT quizName, teacher, duration, categoryName 
    FROM quiz 
    JOIN category ON quiz.categoryId = category.id
    WHERE ${column} = '${value}';
    `
    const res = (await connection.query(sql, [column, value]))[0];
    return res;
}

const getQuestions = async (quizId) => {
    const sql = `
    SELECT * 
    FROM questions
    WHERE quizId = ${quizId};
    `
    const res = await connection.query(sql);
    return res;
}

const getAnswersForQuestion = async (questionId) => {
    const sql = `
    SELECT * 
    FROM possibleAnswers 
    WHERE questionId = ${questionId};
    `
    const res = await connection.query(sql);
    return res;
}

const createQuiz = async (quizName, categoryId, teacher, duration) => {
    const sql = `
        INSERT INTO quiz(quizName, categoryId, teacher, duration)
        VALUES (?,?,?,?)
    `;

    const result = await connection.query(sql, [quizName, categoryId, teacher, duration]);

    return {
        id: result.id,
        quizName: quizName,
        categoryId: categoryId,
        teacher: teacher,
        duration: duration
    };
};

const createAnswersForQuestion = async (questionId, answers) => {
    const insertAnswer = `
    INSERT INTO possibleAnswers(questionId, answer, isCorrect)
    VALUES (?,?,?)
`;

    await answers.forEach(answer => {
        connection.query(insertAnswer, [questionId, answer.answer, answer.isCorrect])
    });
}

const createQuestion = async (quizId, questionText, points, choiceType, answers) => {
    const insertQuestion = `
        INSERT INTO questions(quizId, questionText, points, choiceType)
        VALUES (?,?,?,?)
    `;

    const questionResult = await connection.query(insertQuestion, [quizId, questionText, points, choiceType]);

    return {
        id: questionResult.insertId,
        questionText: questionText,
        answers: answers,
        points: points
    };
};

const deleteQuiz = async (quiz) => {
    const sql = `
    DELETE FROM quiz
    WHERE id = ?
    `;
    return await connection.query(sql, [quiz.id]);
}

const getAllCategories = async () => {
    const sql = `
    SELECT id, categoryName
    FROM category
`;

    return await connection.query(sql);
}

const getCategoryBy = async (column, value) => {
    const sql = `
    SELECT id, categoryName
    FROM category
    WHERE ${column} = '${value}';
    `
    const res = (await connection.query(sql, [column, value]))[0];
    return res;
}

const createCategory = async (categoryName) => {
    const sql = `
    INSERT INTO category (categoryName)
    VALUES (?)
`;

    const result = await connection.query(sql, [categoryName]);

    return {
        id: result.insertId,
        categoryName: categoryName
    };
}

const updateCategory = async (category) => {
    const { categoryName } = category
    const sql = `
    UPDATE category SET
     categoryName = ?
    `;
    return await connection.query(sql, [categoryName]);
}

const deleteCategory = async (category) => {
    const sql = `
    DELETE FROM category
    WHERE id = ?
    `;
    return await connection.query(sql, [category.id]);
}

export default {
    getAllQuizzes,
    getQuizBy,
    getQuizzesByCategory,
    getQuestions,
    getAnswersForQuestion,
    createQuiz,
    createAnswersForQuestion,
    createQuestion,
    deleteQuiz,
    getAllCategories,
    getCategoryBy,
    createCategory,
    updateCategory,
    deleteCategory
}