import express from 'express'
import cors from 'cors'
import userController from './controller/users-controller.js';
import questionsController from './controller/questions-controller.js';
import quizController from './controller/quiz-controller.js';
import categoryController from "./controller/category-controller.js";
import { PORT } from './config.js'
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import YAML from 'yamljs'
import swaggerUI from 'swagger-ui-express';
import examController from './controller/exam-controller.js';

const swaggerJsDoc = YAML.load("./swagger.yaml");
const app = express();
// passport.use(jwtStrategy);
app.use(cors(), express.json());

app.use('/user', userController);
app.use('/question', questionsController);
app.use('/quiz', quizController);
app.use('/category', categoryController);
app.use('/exam', examController);

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerJsDoc));


app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));