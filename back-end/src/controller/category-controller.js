import quizService from "../service/quiz-service.js";
import errorMessage from "../data/error.js";
import express from "express";
import createValidator from "../validation/validator-middleware.js";
import createCategorySchema from '../validation/schemas/create-category.js'
const categoryController = express.Router();
//get all categories
categoryController

    // get category by category Name
    .get('/',
        async (req, res) => {
            const { categoryName } = req.query;
            if (categoryName) {
                const { error, category } = await quizService.getCategoryByName(categoryName);
                if (error) {
                    return res.status(400).send({ message: error });
                } else {
                    return res.status(200).send(category);
                }
            }

            const { error, category } = await quizService.getAllCategories();
            if (error) {
                return res.status(400).send({ message: error });
            } else {
                return res.status(200).send(category);
            }
        })
    //create category
    .post('/',
        createValidator(createCategorySchema),
        async (req, res) => {
            const { categoryName } = req.body;

            const { error, category } = await quizService.createCategory(categoryName);

            if (error) {
                return res.status(400).send({ message: error });
            } else {
                return res.status(201).send(category);
            }
        })
    // update category
    .put('/:category',
        async (req, res) => {
            const { category } = req.params;
            const { id, categoryName } = updateData;

            const result = await quizService.updateCategory();
            if (!result) {
                return res.status(404).send(errorMessage.CATEGORY_ALREADY_EXISTS)
            } else {
                res.status(200).send(result);
            }

        })
    // delete category
    .delete('/:category',
        async (req, res) => {
            const { category } = req.params;
            const deleted = await quizService.deleteCategory(category);
            if (!deleted) {
                return res.status(404).send(errorMessage.CATEGORY_ALREADY_EXISTS);
            } else {
                return res.status(200).send(deleted);
            }


        })
export default categoryController;