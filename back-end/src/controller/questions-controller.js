import express from 'express';
import questionsService from '../service/questions-service.js';
import questionService from '../service/questions-service.js';
import createValidator from "../validation/validator-middleware.js";
import createAnswerSchema from '../validation/schemas/create-answer.js'
import errorMessage from '../data/error.js';

const questionsController = express.Router();

questionsController
    //get question by id
    .get('/:id',
        async (req, res) => {
            const { id } = req.params;
            const { error, question } = await questionsService.getQuestionById(id);
            if (!error) {
                res.status(200).send(question);
            } else {
                res.status(404).send({ message: error });
            }
        })
    .get('/:id/answer',
        async (req, res) => {
            const { id } = req.params;
            const { error, answers } = await questionsService.getAnswersByQuestionId(id);
            if (!error) {
                res.status(200).send(answers);
            } else {
                res.status(404).send({ message: error });
            }
        })
    //Create answer
    .post('/:id/answer',
        createValidator(createAnswerSchema),
        async (req, res) => {
            const { id } = req.params;
            const data = req.body;
            const { error, result, duplicate } = await questionsService.createAnswer(id, data);

            if (!error) {
                res.status(201).send({answer: result, duplicate: duplicate});
            } else {
                res.status(404).send({ message: error });
            }
        })

    //Update question by id
    .put('/:id',
        async (req, res) => {
            const { id } = req.params;
            const updateData = req.body;
            if (!updateData.question) {
                res.status(400).send(errorMessage.QUESTION_ALREADY_EXISTS);
                return;
            }

            const result = await questionService.updateQuestion(+id, updateData);
            if (!result) {
                return res.status(404).send(errorMessage.QUESTION_ALREADY_EXISTS)
            } else {
                res.status(200).send(result);
            }

        })

    //delete question by id
    .delete('/:id',
        async (req, res) => {
            const { id } = req.params;
            const deleted = await questionService.deleteQuestion(id);
            if (!deleted) {
                return res.status(404).send(errorMessage.QUESTION_ALREADY_EXISTS);
            } else {
                return res.status(200).send(deleted);
            }
        })
export default questionsController;