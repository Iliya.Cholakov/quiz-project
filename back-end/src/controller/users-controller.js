import express from 'express';
import errorMessage from '../data/error.js'
import userService from '../service/users-service.js';
import createValidator from "../validation/validator-middleware.js";
import createUserSchema from '../validation/schemas/create-user.js'

export const userController = express.Router();
const sendResult = (foundUser, res) => {
    if (foundUser) {
        return res.status(200).send(foundUser);
    } else {
        return res.status(404).send({ message: errorMessage.NO_SUCH_USER });
    }
}


userController
    // get user by email
    .get('/',
        async (req, res) => {

            const { email } = req.query;

            if (!email) {
                const foundUser = await userService.getAllUsers();
                sendResult(foundUser, res);
            }

            if (email) {
                const foundUser = await userService.getUserBy('email', email);
                sendResult(foundUser, res);
            }
        })

    // get user by Username
    .get('/:username',
        async (req, res) => {
            const { username } = req.params;

            const foundUser = await userService.getUserBy('username', username);
            sendResult(foundUser, res);
        })

    // create User
    .post('/',
        createValidator(createUserSchema),
        async (req, res) => {
            const userData = req.body;

            const { error, user } = await userService.createUser(userData);

            if (error === errorMessage.USER_ALREADY_EXISTS) {
                return res.status(400).send({ message: error });
            } else {
                return res.status(201).send(user);
            }
        })
    .post('/login',
        async (req, res) => {
            const { email, password } = req.body;
            const { error, user } = await userService.userLogin(email, password);

            if (error) {
                res.status(400).send({message: error});
            }

            else {
                const payload = {
                    sub: user.id,
                    email: user.email,
                    role: user.role,
                    firstName: user.firstName,
                    lastName: user.lastName,
                };
                const token = createToken(payload);

                res.status(200).send({token: token});
            }
        })
    // update user
    .put('/:username',
        async (req, res) => {
            const { username } = req.params;
            const updateData = req.body;

            const { error, user } = await userService.updateUser(username, updateData);

            if (error === errorMessage.NO_SUCH_USER) {
                return res.status(404).send({ message: error });
            } else if (error === errorMessage.USERNAME_ALREADY_TAKEN) {
                return res.status(400).send({ message: error });
            } else {
                return res.status(200).send(user);
            }
        })

    // delete user
    .delete('/:username',
        async (req, res) => {
            const { username } = req.params;
            const { error, user } = await userService.deleteUser(username);
            if (error === errorMessage.NO_SUCH_USER) {
                return res.status(404).send({ message: error });
            } else {
                return res.status(200).send(user);
            }
        })

export default userController;