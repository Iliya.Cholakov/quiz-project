import express from 'express';
import quizService from '../service/quiz-service.js'
import createValidator from "../validation/validator-middleware.js";
import createQuestionSchema from '../validation/schemas/create-question.js';
import createQuizSchema from '../validation/schemas/create-quiz.js'

export const quizController = express.Router();

const sendResult = (found, res) => {
    if (found) {
        return res.status(200).send(found);
    } else {
        return res.status(404).send({ message: errorMessage.NO_SUCH_QUIZ });
    }
}

quizController
    //get quiz by category
    .get('/findByCategory',
        async (req, res) => {
            const { categoryName } = req.query;

            const quiz = await quizService.getQuizBy('categoryName', categoryName);

            if (quiz) {
                return res.status(200).send(quiz);
            } else {
                return res.status(404).send({ message: errorMessage.NO_SUCH_QUIZ });
            }
        })

    //get quizzes by id
    .get('/:id',
        async (req, res) => {
            const { id } = req.params;
            const foundQuiz = await quizService.getQuizBy('id', id);
            sendResult(foundQuiz, res)
        })
    // get all questions in quiz
    .get('/:id/question',
        async (req, res) => {
            const { id } = req.params;

            const { error, questions } = await quizService.getQuestions(id);
            if (!error) {
                return res.status(200).send(questions);
            } else {
                return res.status(400).send({ message: error });
            }
        })
    // add question to the quiz
    .post('/:id/question',
        createValidator(createQuestionSchema),
        async (req, res) => {
            const { id } = req.params;
            const question = req.body;

            const { error, newQuestion } = await quizService.createQuestion(id, question);
            if (!error) {
                return res.status(201).send(newQuestion);
            } else {
                return res.status(400).send({ message: error });
            }
        })

    // create Quiz
    .post('/',
        createValidator(createQuizSchema),
        async (req, res) => {
            const quiz = req.body;

            const { error, newQuiz } = await quizService.createQuiz(quiz);

            if (!error) {
                return res.status(201).send(newQuiz);
            } else {
                return res.status(400).send({ message: error });
            }
        })

    //delete quiz by id 
    .delete('/:id',
        async (req, res) => {
            const { id } = req.params;
            const { quiz, error } = await quizService.deleteQuiz(id);
            if (error) {
                return res.status(404).send({ message: error });
            } else {
                return res.status(200).send(quiz);
            }
        })

export default quizController;