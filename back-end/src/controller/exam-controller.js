import examService from "../service/exam-service.js";
import express from 'express'
const examController = express.Router();

examController

    .get('/user/:userId/quiz/:quizId/',
        async (req, res) => {
            const { userId, quizId } = req.params;
            const { error, questions } = await examService.getExam(userId, quizId);
            if (error) {
                return res.status(400).send({ message: error });
            }

            return res.status(200).send(questions);
        })

    .get('/user/:userId/quiz/:quizId/score',
        async (req, res) => {
            const { userId, quizId } = req.params;
            const { error, score } = await examService.getExamScore(userId, quizId);
            if (error) {
                return res.status(400).send({ message: error });
            }

            return res.status(200).send({ score: score });
        })

    // Start Quiz
    .post('/user/:userId/quiz/:quizId',
        async (req, res) => {
            const { userId, quizId } = req.params;
            const { error, message } = await examService.startQuiz(userId, quizId);
            if (error) {
                return res.status(400).send({ message: error });
            }

            return res.status(200).send({ message: message });
        })
    // Submit Quiz
    .post('/user/:userId/quiz/:quizId/submit',
        async (req, res) => {
            const { userId, quizId } = req.params;
            const { answers } = req.body;
            const { error, message, score } = await examService.submitQuiz(userId, quizId, answers);
            if (error) {
                return res.status(400).send({ message: error });
            }

            return res.status(200).send({ message: message, score: score });
        })
export default examController;